# Ansible Role: NFS
Эта роль устанавливает и настраивает nfs-kernel-server на Ubuntu серверы с ролью "nfs_role=server".

Создаёт каталоги указанные в переменной nfs_directories.nfs_server_directory:

    nfs_directories:
      - nfs_server_directory: "/files" #Путь к каталогу, для которого раздается доступ
        nfs_client_directory: "/files" #Путь к каталогу, куда будет монитроваться NFS каталог
        nfs_client_address: "{{ hostvars['web01']['ansible_eth1']['ipv4']['address'] }}" #Указываем адрес сервера или сети которому раздается доступ к каталогу 
        nfs_client_netmask: "{{ hostvars['web01']['ansible_eth1']['ipv4']['netmask'] }}" #Сетевая маска
        nfs_directory_perm: "(rw,insecure,nohide,all_squash,anonuid=1000,anongid=1000)" #Опции

Из этой же переменной создаёт файл /etc/exports.

Эта роль устанавливает и настраивает nfs-common на Ubuntu серверы с ролью "nfs_role=client".
Создаёт каталоги указанные в переменной "nfs_directories.nfs_client_directory".
Монтирует nfs каталог в каталог указанный в nfs_directories.nfs_client_directory

Необходимо заполнить переменную "nfs_server" - адрес NFS сервера